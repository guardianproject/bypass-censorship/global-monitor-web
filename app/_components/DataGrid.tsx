"use client";

import { FC } from "react";
import { Box } from "@mui/material";
import { DataGridPro, GridColDef } from "@mui/x-data-grid-pro";
import { LicenseInfo } from "@mui/x-data-grid-pro";
import { colors, typography } from "../_styles/theme";

LicenseInfo.setLicenseKey(
  "7c9bf25d9e240f76e77cbf7d2ba58a23Tz02NjU4OCxFPTE3MTU4NjIzMzQ2ODgsUz1wcm8sTE09c3Vic2NyaXB0aW9uLEtWPTI="
);

const { white, lightGray, lightMediumGray, darkGray, lightPink } = colors;
const { body } = typography;

type DataGridProps = {
  columns: GridColDef[];
  rows: any[];
  tree?: boolean;
  height?: string;
};

export const DataGrid: FC<DataGridProps> = ({
  columns,
  rows,
  tree = false,
  height = "100%",
}) => (
  <Box
    sx={{
      height,
      backgroundColor: lightMediumGray,
      border: 0,
      width: "100%",
      ".MuiDataGrid-row:hover": { backgroundColor: `${lightPink} !important` },
      ".MuiDataGrid-row:nth-of-type(1n)": {
        backgroundColor: lightGray,
      },
      ".MuiDataGrid-row:nth-of-type(2n)": {
        backgroundColor: white,
      },
      ".MuiDataGrid-columnHeaderTitleContainerContent": {
        margin: "0 auto",
      },
      ".MuiDataGrid-columnHeaderTitle": {
        ...body,
        color: darkGray,
        fontWeight: "bold",
        margin: "0 auto",
      },
      ".MuiDataGrid-columnHeader": {
        backgroundColor: lightMediumGray,
        border: 0,
      },
      ".MuiDataGrid-cell": {
        ...body,
        whiteSpace: "normal",
        wordWrap: "break-word",
        fontSize: 14,
        fontWeight: 500,
        p: "0 !important",
        borderRight: "1px solid #ddd",
      },
    }}
  >
    <DataGridPro
      getTreeDataPath={(row) => [row.country] ?? []}
      defaultGroupingExpansionDepth={1}
      treeData={tree}
      groupingColDef={{
        hideDescendantCount: true,
      }}
      sx={{ height, borderRadius: 0 }}
      rows={rows}
      columns={columns}
      density="compact"
      hideFooter
      rowBuffer={30}
      rowHeight={46}
      scrollbarSize={0}
      disableVirtualization
    />
  </Box>
);
