import { CheckInput } from "../_components/CheckInput";

export const dynamic = "force-dynamic";

export default async function Check() {
  return <CheckInput />;
}
