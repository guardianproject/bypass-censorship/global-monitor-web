"use client";

import { FC, useEffect } from "react";
import { useFormState } from "react-dom";
import { useRouter } from "next/navigation";
import {
  Button,
  Box,
  Grid,
  Select,
  MenuItem,
  TextField,
  Card,
} from "@mui/material";
import { typography } from "app/_styles/theme";
import { updateChecksAction } from "../_actions/checks";
import lookup from "country-code-lookup";

export const CheckInput: FC = () => {
  const { h2, h6 } = typography;
  const router = useRouter();

  const initialState = {
    message: null,
    errors: {},
    success: false,
    values: {
      country: "AF",
      urls: "",
    },
  };
  const [formState, formAction] = useFormState(
    updateChecksAction,
    initialState
  );

  useEffect(() => {
    if (formState.success) {
      router.push("/check/results");
    }
  }, [formState.success, router]);

  return (
    <form action={formAction}>
      <Card sx={{ margin: "0 auto", marginTop: 6, maxWidth: 800, p: 3 }}>
        <Grid container spacing={3} direction="column">
          <Grid item>
            <Box
              sx={{
                ...h2,
                textAlign: "center",
                mb: 1,
                p: 2,
                background: "#f5f5f5",
                border: "1px solid #ccc",
                borderRadius: 1,
              }}
            >
              On-Demand Checker
            </Box>
          </Grid>
          <Grid
            item
            container
            spacing={2}
            sx={{ width: "100%" }}
            alignItems="center"
          >
            <Grid item xs="auto">
              <Box sx={{ ...h6 }}>Country:</Box>
            </Grid>
            <Grid item xs="auto" sx={{ width: "100%" }}>
              <Select
                defaultValue={formState.values.country}
                name="country"
                sx={{ minWidth: "200px" }}
                size="small"
              >
                {lookup.countries.map((c) => (
                  <MenuItem key={c.iso2} value={c.iso2}>
                    {c.country}
                  </MenuItem>
                ))}
              </Select>
            </Grid>
          </Grid>
          <Grid item>
            <TextField
              defaultValue={formState.values.urls}
              name="urls"
              label="URLs (return separated)"
              minRows={10}
              sx={{ width: "100%" }}
              multiline
            />
          </Grid>
          <Grid item container direction="row-reverse">
            <Grid item>
              <Button type="submit" variant="contained" color="primary">
                Submit
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Card>
    </form>
  );
};
