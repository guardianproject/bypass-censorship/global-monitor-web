"use client";

import { FC, useEffect, useState } from "react";
import { Box, Grid, Card, CircularProgress } from "@mui/material";
import { typography } from "app/_styles/theme";
import { fetchChecksAction } from "app/_actions/checks";
import { RecentBlocks } from "./RecentBlocks";
import { RecentChecks } from "./RecentChecks";

export const CheckResults: FC = () => {
  const [firstCheckComplete, setFirstCheckComplete] = useState(false);
  const [checks, setChecks] = useState<any[]>([]);
  const [failing, setFailing] = useState<any[]>([]);
  const { h2, h5 } = typography;
  const height = "300px";

  useEffect(() => {
    console.log("useEffect");
    const fetchChecks = async () => {
      const { checks: c, failing: f } = await fetchChecksAction();
      console.log(c, f);
      setChecks(c);
      setFailing(f);
      setFirstCheckComplete(true);
    };

    let interval = setInterval(fetchChecks, 3000);

    return () => {
      if (interval) clearInterval(interval);
    };
  });

  return (
    <Card sx={{ margin: "0 auto", marginTop: 6, p: 3 }}>
      <Grid container spacing={2} direction="column">
        <Grid item>
          <Box
            sx={{
              ...h2,
              textAlign: "center",
              mb: 1,
              p: 2,
              background: "#f5f5f5",
              border: "1px solid #ccc",
              borderRadius: 1,
            }}
          >
            On-Demand Checker Results
          </Box>
        </Grid>
        <Grid item>
          <Box sx={{ ...h5, mt: 2 }}>Failing Checks</Box>
        </Grid>
        <Grid item>
          {!firstCheckComplete ? (
            <Box sx={{ textAlign: "center", height, mt: "120px" }}>
              <CircularProgress />
            </Box>
          ) : (
            <RecentBlocks recents={failing} height={height} />
          )}
        </Grid>
        <Grid item>
          <Box sx={{ ...h5, mt: 2 }}>All Checks</Box>
        </Grid>
        <Grid item>
          {!firstCheckComplete ? (
            <Box sx={{ textAlign: "center", height, mt: "120px" }}>
              <CircularProgress />
            </Box>
          ) : (
            <RecentChecks recents={checks} height={height} />
          )}
        </Grid>
      </Grid>
    </Card>
  );
};
