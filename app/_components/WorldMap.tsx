"use client";

import React from "react";
import { FC } from "react";
import { Box } from "@mui/material";
import lookup from "country-code-lookup";
import { scaleLinear } from "d3-scale";
import {
  ComposableMap,
  Geographies,
  Geography,
  Sphere,
  Graticule,
} from "react-simple-maps";
import { colors } from "../_styles/theme";

interface MapChartProps {
  countries: any;
}

export const MapChart: FC<MapChartProps> = ({ countries }) => {
  const {
    white,
    lightGray,
    lightMediumGray,
    mediumGray,
    lightPink,
    darkRed,
    yellow,
  } = colors;
  const colorScale = scaleLinear()
    .domain([0, 5, 10])
    .range([lightMediumGray, yellow, darkRed] as any);
  return (
    <Box
      sx={{
        width: 900,
        minHeight: 300,
        margin: "0 auto",
        mt: -14,
        textAlign: "center",
        mb: -14,
      }}
    >
      <ComposableMap
        projectionConfig={{
          rotate: [-10, 0, 0],
          scale: 120,
        }}
      >
        <Sphere
          id="sphere"
          stroke={lightMediumGray}
          strokeWidth={0.5}
          fill={white}
        />
        <Graticule stroke={lightMediumGray} strokeWidth={0.5} />
        {countries.length > 0 && (
          <Geographies geography="/data/features.json">
            {({ geographies }) =>
              geographies.map((geo) => {
                const iso2 = lookup.byIso(geo.id)?.iso2?.toLowerCase();
                const country = countries.find((c: any) => c.country === iso2);

                return (
                  <Geography
                    key={geo.rsmKey}
                    geography={geo}
                    fill={
                      country
                        ? (colorScale(country["risk"]) as any)
                        : lightMediumGray
                    }
                  />
                );
              })
            }
          </Geographies>
        )}
      </ComposableMap>
    </Box>
  );
};
