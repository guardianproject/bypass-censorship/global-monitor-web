"use client";

import { FC } from "react";
import { Box } from "@mui/material";
import { GridColDef } from "@mui/x-data-grid-pro";
import Flag from "react-world-flags";
import lookup from "country-code-lookup";
import { DataGrid } from "./DataGrid";

const treeColumns: GridColDef[] = [
  {
    field: "flag",
    headerName: "",
    flex: 0.25,
    align: "center",
    renderCell: ({ row }) => {
      return (
        <Flag
          code={row.country as string}
          height="16"
          fallback={<span>Unknown</span>}
        />
      );
    },
  },
  {
    field: "country",
    headerName: "Country",
    flex: 2,
    align: "left",
    renderCell: ({ row }) => {
      return (
        <Box
          sx={{
            p: 1.5,
          }}
        >
          {lookup.byIso(row.country)?.country}
        </Box>
      );
    },
  },
  {
    field: "origin",
    headerName: "Origin",
    flex: 4,
    align: "center",
  },
  {
    field: "isp",
    headerName: "ISP",
    flex: 2,
    align: "center",
  },
  {
    field: "error",
    headerName: "Error",
    flex: 2,
    align: "center",
  },
  {
    field: "checkedAt",
    headerName: "Blocked At",
    flex: 4,
    align: "center",
    valueFormatter: ({ value }) =>
      value.toLocaleDateString() + " at " + value.toLocaleTimeString(),
  },
];

interface RecentBlocksProps {
  recents: any;
  height?: string;
}

export const RecentBlocks: FC<RecentBlocksProps> = ({
  recents,
  height = "1000px",
}) => {
  return <DataGrid columns={treeColumns} rows={recents} height={height} />;
};
