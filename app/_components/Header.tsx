import { FC } from "react";
import { Grid, Box } from "@mui/material";
import { colors, typography } from "../_styles/theme";

export const Header: FC = () => {
  const { h1, h5 } = typography;
  const { mediumGray, darkGray } = colors;

  return (
    <Grid container direction="column" alignContent="center" sx={{ mt: 6 }}>
      <Grid item>
        <Box sx={{ ...h5, textAlign: "center", color: mediumGray }}>
          Bypass Censorship
        </Box>
      </Grid>
      <Grid item>
        <Box sx={{ ...h1, textAlign: "center", color: darkGray }}>
          Global Monitor
        </Box>
      </Grid>
    </Grid>
  );
};
