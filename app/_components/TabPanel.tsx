"use client";

import { FC, PropsWithChildren, useState } from "react";
import { Box, Tabs, Tab } from "@mui/material";
import { colors, typography } from "../_styles/theme";

type TabPanelItemProps = PropsWithChildren<{
  index: number;
  value: number;
}>;

const TabPanelItem: FC<TabPanelItemProps> = ({ value, index, children }) => (
  <Box
    role="tabpanel"
    hidden={value !== index}
    id={`simple-tabpanel-${index}`}
    aria-labelledby={`simple-tab-${index}`}
  >
    {value === index && <Box sx={{ p: 0 }}>{children}</Box>}
  </Box>
);

export const TabPanel: FC<PropsWithChildren> = ({ children }) => {
  const { mediumGray, darkGray } = colors;
  const { h6 } = typography;
  const [selectedIndex, setSelectedIndex] = useState(0);
  const handleChange = (_event: any, newValue: number) => {
    setSelectedIndex(newValue);
  };
  const tabStyles = { ...h6, color: mediumGray };

  return (
    <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
      <Tabs
        value={selectedIndex}
        centered
        variant="fullWidth"
        TabIndicatorProps={{
          style: {
            backgroundColor: darkGray,
          },
        }}
        onChange={handleChange}
        sx={{ ".Mui-selected": { color: `${darkGray} !important` } }}
      >
        <Tab label="Blocks by Country" sx={tabStyles} />
        <Tab label="Blocks by Date" sx={tabStyles} />
        <Tab label="Blocks by Origin" sx={tabStyles} />
      </Tabs>
      {(children as any[]).map((child: any, index: number) => (
        <TabPanelItem key={index} value={selectedIndex} index={index}>
          {child}
        </TabPanelItem>
      ))}
    </Box>
  );
};
