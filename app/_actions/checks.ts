"use server";

export const updateChecksAction = async (
  currentState: any,
  formData: FormData,
) => {
  const country = formData.get("country") as string;
  const urls = formData.get("urls") as string;
  let template: any = {
    "version": "1.1",
    "mappings": {},
    "s3_buckets": ["bcp-unknown-logs-cloudfront"]
  };
  const formattedURLs = urls.split("\n").forEach((url) => {
    const urlWithProtocol = url.startsWith("http") ? url : `https://${url}`;
    const parsedURL = new URL(urlWithProtocol);
    const urlWithoutProtocol = parsedURL.href.replace(`${parsedURL.protocol}//`, "");
    template.mappings[urlWithoutProtocol] = {
      origin_domain: urlWithoutProtocol,
      origin_domain_normalized: urlWithoutProtocol,
      origin_domain_root: parsedURL.host,
      valid_from: "2022-01-01T00:00:00.000000",
      valid_to: null,
      country,
      risk: 20
    };
  });

  console.log(formattedURLs);
  const res = await fetch(`${process.env.GM_THREE_URL}/checks`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(template),
  });

  console.log(res);

  return { ...currentState, values: { country, urls }, success: true };
};

export const fetchChecksAction = async (
) => {
  const resOne = await fetch(`${process.env.GM_THREE_URL}/check-results`);
  const checks = await resOne.json();
  const formattedChecks = Object.keys(checks).map((key: any) => {
    return checks[key].map((check: any) => {
      return {
        id: check.checked_at,
        origin: key,
        country: check.desired_country,
        checkedAt: new Date(check.checked_at),
      };
    });
  }).flat().sort((a: any, b: any) => b.checkedAt - a.checkedAt);

  const resTwo = await fetch(`${process.env.GM_THREE_URL}/checks-failing?detailed=true`);
  const failing = await resTwo.json();
  const formattedFailing = Object.keys(failing).map((key: any) => {
    const check = failing[key];
    return {
      id: check.checked_at,
      origin: key,
      error: check.error_type,
      isp: check.isp,
      country: check.country,
      checkedAt: new Date(check.checked_at),
    };

  }).sort((a: any, b: any) => b.checkedAt - a.checkedAt);
  return { checks: formattedChecks, failing: formattedFailing };
};
