import { Box } from "@mui/material";
import { initDb } from "app/_lib/database";
import { MapChart } from "./_components/WorldMap";
import { TabPanel } from "./_components/TabPanel";
import { CountryBlocks } from "./_components/CountryBlocks";
import { RecentBlocks } from "./_components/RecentBlocks";
import { OriginBlocks } from "./_components/OriginBlocks";

export const dynamic = "force-dynamic";

export default async function Home() {
  const db = initDb();

  const offsetDays = parseInt(process.env.OFFSET_DAYS!, 10) || 0;
  const dayOffset = new Date();
  dayOffset.setDate(dayOffset.getDate() - offsetDays);

  const oneDayAgo = new Date(dayOffset.getTime());
  oneDayAgo.setDate(dayOffset.getDate() - 1);

  const threeDaysAgo = new Date(dayOffset.getTime());
  threeDaysAgo.setDate(dayOffset.getDate() - 3);

  const getCountryChecks = (date: Date) => {
    return db
      .selectFrom("checkResult")
      .select(["country"])
      .select(({ fn }) => [fn.count("id").as("checks")])
      .where("checkedAt", ">", date)
      .where("checkedAt", "<", dayOffset) // temp
      .orderBy("checks", "desc")
      .groupBy("country")
      .execute();
  };
  const countryChecks24 = await getCountryChecks(oneDayAgo);
  const countryChecks72 = await getCountryChecks(threeDaysAgo);

  const getCountryBlocks = (date: Date) => {
    return db
      .selectFrom("checkDecision")
      .select(["country"])
      .select(({ fn }) => [fn.count("id").as("blocks")])
      .where("checkedAt", ">", date)
      .where("checkedAt", "<", dayOffset) // temp
      .orderBy("blocks", "desc")
      .groupBy("country")
      .execute();
  };

  const countryBlocks24 = await getCountryBlocks(oneDayAgo);
  const countryBlocks72 = await getCountryBlocks(threeDaysAgo);

  const countries = countryChecks72.map((check: any) => {
    const country = check.country;
    const checks24 =
      countryChecks24.find((check: any) => check.country === country)?.checks ||
      0;
    const checks72 =
      countryChecks72.find((check: any) => check.country === country)?.checks ||
      0;
    const blocks24 =
      countryBlocks24.find((block: any) => block.country === country)?.blocks ||
      0;
    const blocks72 =
      countryBlocks72.find((block: any) => block.country === country)?.blocks ||
      0;

    return {
      id: country,
      country,
      checks24,
      checks72,
      blocks24,
      blocks72,
      risk: Math.min(20, blocks24 as number),
    };
  });
  countries.sort((a: any, b: any) => b.blocks24 - a.blocks24);

  const recents = await db
    .selectFrom("checkDecision")
    .select(["id", "country", "origin", "checkedAt"])
    .where("checkedAt", ">", threeDaysAgo)
    .where("checkedAt", "<", dayOffset) // temp
    .orderBy("checkedAt", "desc")
    .execute();

  const originBlocks = await db
    .selectFrom("checkDecision")
    .select(["origin"])
    .select(({ fn }) => [fn.agg("concat", ["country"]).as("countries")])
    .where("checkedAt", ">", threeDaysAgo)
    .where("checkedAt", "<", dayOffset) // temp
    .groupBy("origin")
    .groupBy("countries")
    .orderBy("origin")
    .execute();

  const origins = originBlocks.map((block: any) => {
    const origin = block.origin;
    const countries = block.countries;

    return {
      id: origin,
      origin,
      countries,
    };
  });

  return (
    <Box>
      <MapChart countries={countries} />
      <TabPanel>
        <CountryBlocks countries={countries} />
        <RecentBlocks recents={recents} />
        <OriginBlocks origins={origins} />
      </TabPanel>
    </Box>
  );
}
