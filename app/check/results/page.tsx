import { CheckResults } from "../../_components/CheckResults";

export const dynamic = "force-dynamic";

export default async function Results() {
  return <CheckResults />;
}
