import { NextRequest, NextResponse } from "next/server";
import crypto from "crypto";
import { initDb } from "app/_lib/database";

export const dynamic = "force-dynamic";

const sourceURLs = [process.env.GM_ONE_URL, process.env.GM_TWO_URL];

const updateResults = async (source: number, path: string, table: string) => {
  const baseURL = sourceURLs[source - 1];
  const url = `${baseURL}/${path}`;
  const response = await fetch(url);
  const json = await response.json();
  const db = initDb();

  for (let [origin, results] of Object.entries(json) as any) {
    if (!Array.isArray(results)) {
      results = [results];
    }
    for (let result of results) {
      const checkedAt = new Date(result.checked_at);
      if (checkedAt < new Date(Date.now() - 20 * 60 * 1000)) {
        console.log("Skipping old result");
        continue;
      }
      console.log(`Updating ${result.checked_at}`);
      const rawKey = `${source}-${result.checked_at}`;
      const id = crypto.createHash("sha256").update(rawKey).digest("hex");
      const exists = await db
        .selectFrom(table as any)
        .where("id", "=", id)
        .executeTakeFirst();

      if (!exists) {
        const insert = await db
          .insertInto(table as any)
          .values({
            id,
            source,
            origin,
            status: result.status,
            errorType: result.error_type,
            checkedAt: result.checked_at,
            country: result.country ?? result.desired_country,
            asn: result.asn ?? result.maxmind_reported_asn,
            proxyProvider: result.proxy_provider,
            isp: result.isp,
          })
          .execute();
      }
    }
  }
};

export const GET = async (req: NextRequest) => {
  if (
    req.headers.get("Authorization") !== `Bearer ${process.env.CRON_SECRET}`
  ) {
    return NextResponse.json({ success: false });
  }

  const updates = [
    {
      source: 1,
      path: "check-results",
      table: "checkResult",
    },
    {
      source: 2,
      path: "check-results",
      table: "checkResult",
    },
    {
      source: 1,
      path: "checks-failing?detailed=true",
      table: "checkDecision",
    },
    {
      source: 2,
      path: "checks-failing?detailed=true",
      table: "checkDecision",
    },
  ];

  for (const update of updates) {
    await updateResults(update.source, update.path, update.table);
  }

  return NextResponse.json({ success: true });
};
