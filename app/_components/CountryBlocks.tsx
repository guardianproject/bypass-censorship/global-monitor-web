"use client";

import { FC } from "react";
import { GridColDef } from "@mui/x-data-grid-pro";
import { DataGrid } from "./DataGrid";
import { Box } from "@mui/material";
import lookup from "country-code-lookup";
import Flag from "react-world-flags";
import { colors } from "../_styles/theme";
import { scaleLinear } from "d3-scale";

const { darkRed, yellow, transparent } = colors;
const colorScale = scaleLinear()
  .domain([0, 5, 10])
  .range([transparent, `${yellow}cc`, `${darkRed}cc`] as any);
const treeColumns: GridColDef[] = [
  {
    field: "flag",
    headerName: "",
    flex: 0.25,
    align: "center",
    renderCell: ({ row }) => {
      return (
        <Flag
          code={row.country as string}
          height="16"
          fallback={<span>Unknown</span>}
        />
      );
    },
  },
  {
    field: "country",
    headerName: "Country",
    flex: 1.5,
    align: "left",
    renderCell: ({ row }) => {
      return (
        <Box
          sx={{
            p: 1.5,
          }}
        >
          {lookup.byIso(row.country)?.country}
        </Box>
      );
    },
  },
  {
    field: "risk",
    headerName: "Risk",
    flex: 1,
    align: "center",
    renderCell: ({ row }) => {
      return (
        <Box
          sx={{
            backgroundColor: colorScale(row.risk as number),
            width: "100%",
            p: 2,
            textAlign: "center",
          }}
        >
          {row.risk as number}
        </Box>
      );
    },
  },
  {
    field: "checks24",
    headerName: "Checks (24h)",
    flex: 1,
    align: "center",
  },
  {
    field: "blocks24",
    headerName: "Blocks (24h)",
    flex: 1,
    align: "center",
  },
  {
    field: "checks72",
    headerName: "Checks (72h)",
    flex: 1,
    align: "center",
  },
  {
    field: "blocks72",
    headerName: "Blocks (72h)",
    flex: 1,
    align: "center",
  },
];

interface CountryBlocksProps {
  countries: any;
}

export const CountryBlocks: FC<CountryBlocksProps> = ({ countries }) => {
  return <DataGrid columns={treeColumns} rows={countries} height="1000px" />;
};
