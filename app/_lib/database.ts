import { CamelCasePlugin } from "kysely";
import { createKysely } from "@vercel/postgres-kysely";

interface CheckResultTable {
  id: number;
  origin: string;
  status: string;
  errorType: string;
  checkedAt: Date;
  country: string;
  asn: number;
  proxyProvider: string;
  isp: string;
  source: number;
}

interface CheckDecisionTable {
  id: number;
  origin: string;
  status: string;
  errorType: string;
  checkedAt: Date;
  country: string;
  asn: number;
  proxyProvider: string;
  isp: string;
  source: number;
}

interface Database {
  checkResult: CheckResultTable; 
    checkDecision: CheckDecisionTable;
}

export const initDb = () => {
  return createKysely<Database>(undefined, {
    plugins: [new CamelCasePlugin()],
  });
};
