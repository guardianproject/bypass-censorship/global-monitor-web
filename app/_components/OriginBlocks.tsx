"use client";

import { FC } from "react";
import { Box } from "@mui/material";
import { GridColDef } from "@mui/x-data-grid-pro";
import Flag from "react-world-flags";
import lookup from "country-code-lookup";
import { DataGrid } from "./DataGrid";

const treeColumns: GridColDef[] = [
  {
    field: "origin",
    headerName: "Origin",
    flex: 4,
    align: "center",
  },
  {
    field: "flag",
    headerName: "",
    flex: 0.25,
    align: "center",
    renderCell: ({ row }) => {
      return (
        <>
          {row.countries?.split(",").map((c: string) => (
            <Flag
              key={c}
              code={c}
              height="16"
              fallback={<span>Unknown</span>}
            />
          ))}
        </>
      );
    },
  },
  {
    field: "countries",
    headerName: "Countries",
    flex: 2,
    align: "left",
    renderCell: ({ row }) => {
      return (
        <Box
          sx={{
            p: 1.5,
          }}
        >
          {row.countries
            .split(",")
            .map((c: any) => lookup.byIso(c)?.country ?? "")
            .join(", ")}
        </Box>
      );
    },
  },
];

interface OriginBlocksProps {
  origins: any;
}

export const OriginBlocks: FC<OriginBlocksProps> = ({ origins }) => {
  return <DataGrid columns={treeColumns} rows={origins} height="1000px" />;
};
