import { IBM_Plex_Sans } from "next/font/google";

const ibmPlexSans = IBM_Plex_Sans({
  subsets: ["latin"],
  weight: ["400", "700"],
});
const { fontFamily } = ibmPlexSans.style;

export const colors = {
  white: "#ffffff",
  lightGray: "#f3f3f3",
  lightMediumGray: "#e0e0e0",
  mediumGray: "#a0a0a0",
  darkGray: "#4d4d4d",
  lightPink: "#ffedea",
  darkRed: "#ff5233",
  yellow: "#ffc641",
  transparent: "transparent",
};

export const typography = {
  h1: {
    fontSize: 44,
    lineHeight: 1.3,
    fontWeight: 700,
    fontFamily,
  },
  h2: {
    fontSize: 24,
    lineHeight: 1.3,
    fontWeight: 700,
    fontFamily,
  },
  h5: {
    fontSize: 16,
    lineHeight: 1,
    textTransform: "uppercase",
    fontWeight: 700,
    fontFamily,
  },
  h6: {
    fontSize: 14,
    textTransform: "none",
    lineHeight: 1,
    fontWeight: 700,
    fontFamily,
  },
  body: {
    fontSize: 12,
    lineHeight: 1,
    fontFamily,
  },
};
