import type { Metadata } from "next";
import { Stack, Container, Box } from "@mui/material";
import { IBM_Plex_Sans } from "next/font/google";
import { Header } from "./_components/Header";
import { colors } from "./_styles/theme";
import "./_styles/globals.css";

const { lightGray } = colors;

const ibmPlexSans = IBM_Plex_Sans({
  subsets: ["latin"],
  weight: ["400", "700"],
});

export const metadata: Metadata = {
  title: "Global Monitor",
  description: "Global Monitor",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body
        className={ibmPlexSans.className}
        style={{
          backgroundColor: lightGray,
        }}
      >
        <Container>
          <Stack>
            <Header />
            <Box>{children}</Box>
          </Stack>
        </Container>
      </body>
    </html>
  );
}
